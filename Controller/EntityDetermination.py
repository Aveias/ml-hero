#  -*-coding:utf8 -*

from anytree.search import find

class EntityDetermination:
    """Détermine si le critère de recherche d'un arbre est true ou false pour une entité"""
    def __init__(self, tree, entity):
        self.entity = entity
        self.reference = tree.reference



        # On démarre les recherches à partir du noeud initial
        self._find_next_node(tree.nodes[0])

    def _find_next_node(self, current_node):
        """Identifie la valeur de l'entité pour le critère courant et retourne le suivant, ou le résultat"""
        # On identifie la valeur de l'entité pour le critère courant
        criteria_value = self.entity[current_node.name]

        # Pour ce critère, on récupère le noeud enfant
        node = find(current_node, lambda node: node.name == criteria_value and node.parent == current_node)

        # Si cette valeur n'est pas référencée, on est bien incapable de déterminer quoi que ce soit
        if node == None:
            self._print_result("inconnue. Que voulez-vous, même la technologie a ses limites...")
        else:
            next_node = node.children[0]

            # Si le gamin de ce noeud est true ou false : on a un résultat
            if next_node.name in ("True", "False"):
                self._print_result(next_node.name)
            #  sinon on recommence
            else:
                self._find_next_node(next_node)

    def _print_result(self, result):
        """Affiche le résultat"""
        print("Notre ami.e", self.entity["Nom"], "est elle de type", self.reference, "?", "\nLa réponse est",
              result)

# Hero

Super programme en Python qui dit si Moustache est un Héros ou pas. Mais si on change un peu le tableau de données et le critère recherché, ça marche aussi. Normalement.

# Prérequis

Il faudra anytree pour faire tourner le bouzin : `pip install anytree` (ou `pip3 install anytree`)
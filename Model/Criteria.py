# -*-coding:utf8 -*

from math import log


class Criteria :
    """Classe Critère - valeurs d'un critère pour l'éléments recherché"
        Attributs :
        @string name = nom du critère
        @dict{@dict{@int} values = totaux des correspondances des différentes valeurs pour ce critère

    """

    def __init__(self, criteriaList, referenceList):
        """Constructeur - liste les valeurs possibles du critère """
        # Definition du nom et suppression des header de la liste
        self.name = criteriaList[0]
        self.referenceList = referenceList.copy()
        self.criteriaList = criteriaList.copy()
        del self.criteriaList[0]
        del self.referenceList[0]

        # Définition des différentes valeurs
        self.values = {}
        for key, value in enumerate(self.criteriaList):
            # Ajout de la valeur si nouvelle
            if value not in self.values.keys():
                self.values[value] = {"True": 0, "False": 0}
            # Et incrément de la valeur du critère correspondant à l'élément de référence recherché
            if self.referenceList[key] == "O" or self.referenceList[key] is True or self.referenceList[key] == "Oui":
                self.values[value]["True"] += 1
            elif self.referenceList[key] == "N" or self.referenceList[key] is False or self.referenceList[key] == "Non":
                self.values[value]["False"] += 1

    def get_entropy(self):
        """Calcule et retourne l'entropie du critère"""
        # On récupère les totaux
        totals = {"all": 0}
        for key, value in self.values.items():
            totals[key] = value["True"] + value["False"]
            totals["all"] += totals[key]

        # Maintenant qu'on a tout ce qu'il nous faut, on peut lancer le calcul !
        entropy = 0
        for key, value in self.values.items():
            entropy += totals[key]/totals["all"] \
                       * ((self._entropy_frag(value["True"]/totals[key]))
                          + (self._entropy_frag(value["False"]/totals[key])))

        return entropy

    @staticmethod
    def _entropy_frag(prob):
        """Un morceau du calcul d'entropie, permet d'alléger la formule"""
        if prob == 0:
            return 0
        return -1 * prob * (log(prob)/log(2))

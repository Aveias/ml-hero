# -*-coding:utf8 -*


class Table:

    """Classe Tableau. Instancie un tableau de données manipulable"""

    def __init__(self, table):
        self.table = table

    def get_column(self, key):
        """retourne toutes les valeurs d'une colonne sous la forme d'une liste"""
        column = list()
        for i, entry in enumerate(self.table):
                column.append(entry[key])
        return column

    def remove_criteria(self, criteria, value):
        """Génère un nouveau tableau à partir d'un autre
        en enelvant les données liées à un critère et en ne gardant que la valeur de ce critère"""
        newTable = []

        #  récupère l'indice du critère à virer
        index = self.table[0].index(criteria)

        # on vire allègrement cet indice de toutes les entrées du tableau qui matchent et on les ajoute au nouveau
        for entry in self.table:
            if entry[index] == value or entry[index] == criteria:
                newEntry = entry.copy()
                del newEntry[index]
                newTable.append(newEntry)

        return newTable

    def get_criteria_list(self, reference=""):
        """Retourne la liste des critères à analyser dans un tableau,
         en ignorant une colonne de référence si spécifiée"""
        # On parcourt la ligne d'entête pour récupérer les colonnes à scanner
        toScan = []
        for i, header in enumerate(self.table[0]):
            # on ne prend pas la colonne nom qui est inutile, ni la colonne de référence
            if i != 0 and header != reference:
                toScan.append(self.get_column(i))
        return toScan

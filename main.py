#  -*-coding:utf8 -*

from anytree import RenderTree
from Model.DecisionTree import DecisionTree
from Controller.EntityDetermination import EntityDetermination

# Données initiales
data = [
        ["Nom",             "Cape", "Argent",   "Tech", "Pouvoir",  "Héro"],
        ["Spiderman",       "N",    "N",        "N",    "O",        "O"],
        ["Poutine",         "N",    "O",        "O",    "?",        "N"],
        ["Batman",          "O",    "O",        "O",    "N",        "O"],
        ["Jocker",          "N",    "O",        "O",    "N",        "N"],
        ["Rorschach",       "N",    "N",        "N",    "?",        "O"],
        ["Deadpool",        "N",    "N",        "O",    "O",        "O"],
        ["Merckel",         "N",    "O",        "O",    "N",        "N"],
        ["D'Artagnan",      "O",    "N",        "N",    "N",        "N"],
        ["César",           "O",    "O",        "O",    "N",        "N"],
        ["Tesla",           "N",    "N",        "O",    "?",        "O"],
        ["Edison",          "N",    "O",        "O",    "N",        "N"],
        ["Homer Simpson",   "N",    "N",        "N",    "N",        "N"],
        ["Sherlock Holmes", "N",    "O",        "N",    "?",        "O"],
        ["Moriarty",        "N",    "O",        "O",    "?",        "N"]
       ]

""" Tableau de data secondaire. Utile pour faire des tests sur la première partie du cours
dataGolf = [
["Jour", "Climat", "Température", "Humidité", "Vent", "Golf"],
["1   ", "Pluie ", "+", "+", "Non ", "N"],
["2   ", "Pluie ", "+", "+", "Oui ", "N"],
["3   ", "Nuage ", "+", "+", "Non ", "O"],
["4   ", "Soleil", "~", "+", "Non ", "O"],
["5   ", "Soleil", "-", "~", "Non ", "O"],
["6   ", "Soleil", "-", "~", "Oui ", "N"],
["7   ", "Nuage ", "-", "~", "Oui ", "O"],
["8   ", "Pluie ", "~", "+", "Non ", "N"],
["9   ", "Pluie ", "-", "~", "Non ", "O"],
["10  ", "Soleil", "~", "~", "Non ", "O"],
["11  ", "Pluie ", "~", "~", "Oui ", "O"],
["12  ", "Nuage ", "~", "+", "Oui ", "O"],
["13  ", "Nuage ", "+", "~", "Non ", "O"],
["14  ", "Soleil", "~", "+", "Oui ", "N"]
]
"""
# On détermine notre critère de référence : celui qu'on va chercher à déterminer
# On peut aussi directment passer une chaine de caractère si jamais
# elle doit juste correspondre à un header du tableau (ex : Héro)
# Le critère doit aussi contenir des valeurs "propres" : oui/non, o/n, True/False...
reference = data[0][5]

# Création de l'arbre et affichage (ça fait toujours plaisir après s'être saoûlé à le construire)

tree = DecisionTree(data, reference)
print("ooooh, le bel arbre de décision ! \n", RenderTree(tree.nodes[0]).by_attr("name"),
      "\n==============================================================================\n")

# Nouvelle entité : est-ce un héro ?
# Idéalement il faudrait rentrer les infos à la mano via
# un prompt interactif qui demande selon l'arbre décisionnel,
# pis ajouter les entités ainsi récoltées à la base
# pour affiner les résultats,
# mais bon j'ai un mémoire à faire alors c'est en dur dans le code
entity = {
    "Nom": "Moustache",
    "Cape": "N",
    "Argent": "N",
    "Tech": "O",
    "Pouvoir": "?",
    "Héro": "?"
}

EntityDetermination(tree, entity)

input("\n\nAppuyez sur Entrée pour fermer le programme...")
